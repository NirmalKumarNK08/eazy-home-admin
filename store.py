from passlib.hash import sha256_crypt as sha256
from getpass import getpass
from matplotlib import use
from sklearn import linear_model
from matplotlib import pyplot as plt
import numpy as np
import os
import yaml
import pandas as pd
from datetime import datetime as dt
from datetime import timedelta
import sys

use('Agg')  # For matplotlib backend

serverUPTime = dt.now()


def add_devices(name, pin, type_dev='passive'):
    with open(r'devices.yaml', 'r') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    data.pop('file_updated_on', None)
    pin = int(pin)
    try:
        d = data[name]
        print("Device already Exist: " + str(d))
        f.close()
    except:
        try:
            count = len(data)
        except:
            count = 0
        f.close()
        dict_devices = dict()
        dict_devices[name] = dict()
        dict_devices[name]['type'] = type_dev
        dict_devices[name]['added_on'] = dt.now()
        dict_devices[name]['updated_on'] = dt.now()
        dict_devices[name]['status'] = 'off'
        dict_devices[name]['id'] = count + 1
        dict_devices[name]['usage'] = 0
        dict_devices[name]['pin'] = pin

        with open(r'devices.yaml', 'a+') as f:
            yaml.dump(dict_devices, f)


def create_user(username, password):
    with open('user.yaml') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    f.close()
    try:
        x = data[username]
        f.close()
        if x is not None:
            print("User already Exist.")
    except:
        hashedpassword = sha256.encrypt(password)
        dict_p = dict()
        dict_p[username] = dict()
        dict_p[username]['settings'] = 'default'
        dict_p[username]['password'] = hashedpassword
        with open(r'user.yaml', 'a+') as f:
            yaml.dump(dict_p, f)
        f.close()


def verify_user(username, password):
    with open('user.yaml') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    userdata = data[username]
    if userdata:
        res = sha256.verify(password, userdata['password'])
        if res:
            print("Password verified as correct ")
            f.close()
            return True
        else:
            print("Wrong password")
            f.close()
            return False
    else:
        f.close()
        return None


def list_devices_name():
    # function to list all devices
    with open(r'devices.yaml', 'r') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    f.close()
    data.pop('file_updated_on', None)  # remove unneeded key
    return tuple(data.keys())


def change_device_status(device, status):
    # function to change device status
    l_devices = list_devices_name()
    if device in l_devices:
        with open('devices.yaml') as f:
            data = yaml.load(f, Loader=yaml.FullLoader)
        data[device]['status'] = status
        if status == 'off':
            diff = dt.now() - data[device]['updated_on']
            days, sec = diff.days, diff.seconds
            hour = (days * 24) + (sec / 60) / 60
            data[device]['usage'] += hour

        data[device]['updated_on'] = dt.now()
        data['file_updated_on'] = dt.now()
        with open('devices.yaml', 'w') as f:
            yaml.dump(data, f)
        if serverUPTime is not None:
            dataDays = data['file_updated_on'] - serverUPTime
            if dataDays.days > 30:
                createDataFile(dt.now() - timedelta(days=30))

        return 'Device status changed.'
    else:
        return "Device not found in the file. Please update the file. Else the data will be inconsistent."


def get_device_status(name="all"):
    # function to get a device status. If argument not provided it will return all devices
    l_devices = list_devices_name()
    with open(r'devices.yaml', 'r') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    mylist = list()
    if name == "all":
        for i in l_devices:
            mylist.append((i, data[i]['status']))
        return tuple(mylist)
    else:
        if name in l_devices:
            return data[name]['status']
        else:
            return None


def get_device_details(name):
    with open(r'devices.yaml', 'r') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    keys = list(data.keys())
    if name in keys:
        return [data[name]['id'], name, data[name]['updated_on'], data[name]['status'], data[name]['type']]
    else:
        return False


def list_devices_name_detailed():
    # function to list all devices with Details
    with open(r'devices.yaml', 'r') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    keys = list_devices_name()
    mylist = []
    for i in keys:
        mylist.append((data[i]['id'], i, data[i]['updated_on'], data[i]['status'], data[i]['type']))
    return mylist


def change_all_device_status(status):
    # function to change all device status
    l_devices = list_devices_name()
    for i in l_devices:
        change_device_status(i, status)


def get_all_devices_usage():
    # function to get all devices status
    l_devices = list_devices_name()
    with open(r'devices.yaml', 'r') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    mylist = list()
    for i in l_devices:
        mylist.append((i, data[i]['usage']))
    return tuple(mylist)


def graph(date='cur', chart_type="pie"):
    print('I am being used.')
    if date == 'cur':
        usage = get_all_devices_usage()
        xaxis, yaxis = list(), list()
        for i in range(len(usage)):
            xaxis.append(usage[i][0])
            yaxis.append(int(usage[i][1]))
        print(xaxis)
        print(yaxis)
        if chart_type == "pie":
            fig = plt.figure()
            ax = fig.add_axes([0, 0, 1, 1])
            ax.axis('equal')
            ax.pie(yaxis, labels=xaxis, autopct='%1.2f%%')
            fig.savefig('./static/graphs/piechart_summary.png')
        else:
            plt.bar(xaxis, yaxis)
            plt.title('current total usage')
            plt.xlabel('devices')
            plt.ylabel('usage (hrs)')
            if not os.path.exists('static/graphs/charts'):
                os.mkdir('./static/graphs/charts')
            plt.savefig('./static/graphs/charts/barchart_summary.png')

    else:
        print(date)
        split = date.split('-')
        print(split)
        f_path = get_graph_data(str(split[0]), str(split[1]))
        if f_path != '':
            with open(f'{f_path}', 'r') as f:
                data = yaml.load(f, Loader=yaml.FullLoader)
            data.pop("total-usage", None)
            xaxis, yaxis = list(), list()
            for i in data.keys():
                xaxis.append(i)
                yaxis.append(data[i])
            if chart_type == "pie":
                fig = plt.figure()
                ax = fig.add_axes([0, 0, 1, 1])
                ax.axis('equal')
                ax.pie(yaxis, labels=xaxis, autopct='%1.2f%%')
                if not os.path.exists('static/graphs/charts'):
                    os.mkdir('./static/graphs/charts')
                fig.savefig('./static/graphs/charts/piechart_summary.png')
            else:
                plt.bar(xaxis, yaxis)
                plt.title('current total usage')
                plt.xlabel('devices')
                plt.ylabel('usage (hrs)')
                if not os.path.exists('static/graphs/charts'):
                    os.mkdir('./static/graphs/charts')
                plt.savefig('./static/graphs/charts/barchart_summary.png')
            return True
        else:
            return False


def get_graph_data(month, year):
    title = f'{month}-{year}'
    if os.path.exists(f'./static/data/{title}.yaml'):
        return f'./static/data/{title}.yaml'
    else:
        return ''


def createDataFile(date):
    Month = date.month
    Year = date.year
    with open('devices.yaml', 'r') as g:
        old_data = yaml.load(g, Loader=yaml.FullLoader)
    new_data = old_data
    file_data = dict()
    total_usage = 0
    l_devices = list_devices_name()
    for i in l_devices:
        file_data[i] = old_data[i]['usage']
        total_usage += old_data[i]['usage']
        new_data[i]['usage'] = 0
    title = f'{Month}-{Year}'
    with open(f'./static/data/{title}.yaml', 'w+') as f:
        yaml.dump(file_data, f)
    f.close()
    with open('devices.yaml', 'w+') as f:
        yaml.dump(new_data, f)


def create_config_file():
    data = dict()
    data['use-https'] = True
    data['port'] = 5000
    data['debug'] = False
    data['server_start_time'] = dt.now()
    data['use-telegram-api'] = False
    data['send-sms'] = False
    data['arduinoport'] = '/dev/ttyACM0'
    data['mode'] = 'legacy'
    with open('config.yaml', 'w') as f:
        yaml.dump(data, f)


def truncate_file(file_name='devices.yaml'):
    data = dict()
    with open(file_name, 'w')as f:
        yaml.dump(data, f)
    data['use-automated-doors'] = False


def change_password(username, password):
    # Function to change password of user
    with open('user.yaml') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    f.close()
    try:
        if data[username]:
            hashed_password = sha256.hash(password)
            data[username]['password'] = hashed_password
            with open('user.yaml', 'w') as f:
                yaml.dump(data, f)
                return True
    except IndexError:
        return False


def change_config(arduinoport, debug, port, use_telegram, use_automatic_door, use_https):
    """
        Changes the configuration file. If the string is empty this won't change the property.
        :param arduinoport: valid arduino port
        :param debug: set whether the server to start in debug mode
        :param port: set the server port
        :param use_telegram: enables telegram module
        :param use_automatic_door: sets the value of automatic door usage
        :param use_https: set the server to start in HTTPS
     
    """
    with open('config.yaml') as f:
        config = yaml.load(f, Loader=yaml.FullLoader)
    if arduinoport != '':
        config['arduinoport'] = arduinoport
    if port != '':
        config['port'] = int(port)
    if debug != '':
        config['debug'] = convert_string_to_boolean(debug)
    if use_telegram != '':
        config['use-telegram-api'] = convert_string_to_boolean(use_telegram)
    if use_automatic_door != '':
        config['use-automated-door'] = convert_string_to_boolean(use_automatic_door)
    if use_https != '':
        config['use-https'] = convert_string_to_boolean(use_https)
    with open('config.yaml', 'w') as f:
        yaml.dump(config, f)


def getConfig():
    """
    :return: tuple of configuration file.
    """
    try:
        with open('config.yaml', ) as f:
            config = yaml.load(f, Loader=yaml.FullLoader)
        return tuple([config['arduinoport']
                         , config['use-telegram-api']
                         , config['debug']
                         , config['port']
                         , config['use-automated-door']
                         , config['use-https']
                         , config['mode']
                         , config['Door-pin']])
    except FileNotFoundError:
        return tuple()


def convert_string_to_boolean(str):
    if str == 'true' or str == 'True':
        return True
    elif str == 'false' or str == 'False':
        return False


def remove_device(device):
    with open(r'devices.yaml', 'r') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    val = data.pop(device, None)
    if val is not None:
        with open(r'devices.yaml', 'w') as f:
            yaml.dump(data, f)
        return 'Deleted'
    else:
        return 'Device not found'


def get_pin_number(devname):
    l_devices = list_devices_name()
    config = getConfig()
    if devname in l_devices:
        print('exist')
        with open('devices.yaml', 'r') as f:
            data = yaml.load(f, Loader=yaml.FullLoader)
        print(data[devname]['pin'])
        return data[devname]['pin']
    elif devname == "Door" or devname == "door":
        return int(config[7])
    else:
        return None


# functions to draw regression.
def get_labels(directory='./static/data'):
    x_values = []
    y_values = []
    x_label = []
    inc = 30
    all_names = []
    all_date_objects = []
    splitStr = []

    for root, dirs, files in os.walk(directory, topdown=False):
        for name in files:
            if name.endswith('.yaml'):
                all_names.append(name)

    for name in all_names:
        splitStr = name.split('-')
        splitStr[1] = splitStr[1].split('.')[0]
        actual_date = dt(int(splitStr[1]), int(splitStr[0]), 28)
        all_date_objects.append(actual_date)

    all_date_objects = sorted(all_date_objects)

    for date in all_date_objects:
        string = date.strftime("%m-%Y")
        x_label.append(string)

    for name in x_label:
        try:
            if os.path.exists(f'{directory}/{name}.yaml'):
                with open(f'{directory}/{name}.yaml', 'r') as f:
                    data = yaml.load(f, Loader=yaml.FullLoader)
                y_values.append(int(data.pop("total-usage", 0)))
                x_values.append(inc)
                inc += 30
        except Exception as e:
            print(e)

    return x_values, y_values, x_label


def get_predictions(value):
    x_values, y_values, _lables = get_labels()
    x = merge_array(x_values, y_values)
    dataframe = pd.DataFrame(x, columns=['days', 'hours'])
    x_values = dataframe['days'].values[:, np.newaxis]
    y_values = dataframe['hours'].values[:, np.newaxis]
    body_reg = linear_model.LinearRegression()
    body_reg.fit(x_values, y_values)
    predict_values = np.array(value, dtype=np.float64).reshape(1, -1)
    real_predictions = body_reg.predict(predict_values)
    print(f'The predicted values are: {real_predictions}')
    return real_predictions


def draw_regression(v, show=False):
    if v > 0:
        x_values, y_values, x_label = get_labels()
        x_values.append(v)
        old_x = x_values
        if v - x_values[len(x_values) - 1] < 365:
            my_preditions = get_predictions([v])[0][0]
            y_values.append(int(my_preditions))
            old_y = y_values
            new_x = [x_values[len(x_values) - 2]]
            new_y = [y_values[len(y_values) - 2]]
            new_x_label = [x_label[len(x_label) - 1]]
            string = x_label[len(x_label) - 1]
            splitStr = string.split('-')
            splitStr[1] = splitStr[1].split('.')[0]
            actual_date = dt(int(splitStr[1]), int(splitStr[0]), 28)
            predictedMonthName = actual_date + timedelta(days=(v - x_values[len(x_values) - 2]))
            print(v - x_values[len(x_values) - 1])
            predictedMonthName = predictedMonthName.strftime("%m-%Y")
            x_label.append(predictedMonthName)
            x = merge_array(x_values, y_values)
            dataframe = pd.DataFrame(x, columns=['days', 'hours'])
            x_values = dataframe['days'].values[:, np.newaxis]
            y_values = dataframe['hours'].values[:, np.newaxis]
            body_reg = linear_model.LinearRegression()
            body_reg.fit(x_values, y_values)
            plt.scatter(x_values, y_values)
            plt.plot(x_values, y_values, "red")
            new_x.append(v)
            new_y.append(my_preditions)
            plt.scatter(new_x, new_y)
            plt.plot(new_x, new_y, "green")
            plt.xlabel('days')
            plt.ylabel('total hours of usage')
            plt.title("Your overall applaince usage statistics")
            if not os.path.exists('./static/graphs'):
                os.mkdir('./static/graphs')
            if not os.path.exists('./static/graphs/plot_regression'):
                os.mkdir('./static/graphs/plot_regression')
            plt.savefig("./static/graphs/plot_regression/linearreg.png")
            if show:
                plt.show()
            # m = merge_array(old_x, x_label)
            # TODO: MERGE THE old_x,x_label,old_y
            mylist = []
            for i in range(len(old_x)):
                mylist.append((old_x[i], x_label[i], old_y[i]))
            return mylist, old_x[len(old_x) - 1], x_label[len(old_x) - 1]
        else:
            print("Value cannot be zero/null")
            raise ValueError()


def merge_array(x_values, y_values):
    merged_array = list(zip(x_values, y_values))
    for i in range(len(merged_array)):
        merged_array[i] = list(merged_array[i])
    return merged_array


def showRegressionFor30days():
    x_values, _y, _xl = get_labels()
    v = x_values[(len(x_values) - 1)] + 30
    print(x_values[(len(x_values) - 1)] + 30)
    return draw_regression(v, False)


if __name__ == '__main__':
    # Command Line Args to help users configure the storage backend
    if not os.path.exists('config.yaml'):
        create_config_file()
    if len(sys.argv) > 1:
        if sys.argv[1] == 'verify' or sys.argv[1] == '-v':
            x = input('Enter the username: ')
            y = getpass('Enter the password: ')
            verify_user(x, y)
        elif sys.argv[1] == 'user' or sys.argv[1] == '-u':
            x = input('Enter the username: ')
            y = getpass('Enter the password: ')
            create_user(x, y)
        elif sys.argv[1] == '-cp':
            y = getpass('Enter the new password of ' + sys.argv[2] + ': ')
            status = change_password(sys.argv[2], y)
            if not status:
                print('User record not exist')
        elif sys.argv[1] == 'device' or sys.argv[1] == '-a':
            # Add devices
            name = input('Enter the name: ')
            # type_dev = input('Enter the type of device (Default : passive): ')
            pin_number = input("Enter the pin number:")
            add_devices(name, pin_number)
        elif sys.argv[1] == '-r':
            remove_device(sys.argv[2])
        elif sys.argv[1] == '-ld':
            # List all devices
            devices = list_devices_name_detailed()
            # print(devices)
            t_device = tuple(devices)
            print(t_device)
        elif sys.argv[1] == '-cs':
            # Change device status
            x = input('Enter the device name: ')
            y = input('Enter the status: ')
            msg = change_device_status(x, y)
        elif sys.argv[1] == '-gs':
            print(get_device_status())
        elif sys.argv[1] == '-t':
            # truncate_file
            truncate_file(sys.argv[2])
        else:
            print("-v verify : Verify Username and password")
            print("-u user: add new user (Password is changed if the user account already exists.)")
            print("-a device: add new device")
            print("-ld: list all devices")
            print("-cs: change device status")
            print("-gs: get all devices status")
            print("-rd: remove the devices")
            print("-t  devices.yaml/user.yaml/config.yaml: truncate the configuration file. If file name not given "
                  "the devices.yaml will be truncated.")
            print("-cp <username>: Change Password of a user")
    else:
        print("-v verify : Verify Username and password")
        print("-u user: add new user (Password is changed if the user account already exists.)")
        print("-a device: add new device")
        print("-ld: list all devices")
        print("-cs: change device status")
        print("-gs: get all devices status")
        print("-rd: remove the devices")
        print(
            "-t  devices.yaml/user.yaml/config.yaml: truncate the configuration file. If file name not given the "
            "devices.yaml will be truncated.")
        print("-cp <username>: Change Password of a user")
