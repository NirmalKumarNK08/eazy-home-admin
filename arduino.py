"""
* This is a very simple code used for communication with arduino (legacy mode).
* Check out the testArduinoCode on how to use this.
* This can be used when you want a custom arduino code. (Recommended For arduino Developers)
* The arduino code can be tailored as you wish depending on amount of devices that you are going to control using it.
* For configuration. Fill in the fields given on arduino sketch and assign those devices on "devices.yaml" file using web-interface.
* You can also edit the devices.yaml file but be sure to follow the format.
"""

import sys
import serial
import yaml
import time
import os
from store import create_config_file

data = None
if os.path.exists('./config.yaml'):
    with open('./config.yaml') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
else:
    create_config_file()


def list_serial_ports():
    import serial.tools.list_ports
    """ Lists serial port names

        :returns:
            A list of the serial ports available on the system
    """
    ports = list(serial.tools.list_ports.comports())
    return ports


def arduino_write(s, data):
    try:
        s.write(data.encode())
        x = s.readline().decode('utf-8')
        print(x)
        return x, True
    except Exception as e:
        print(e)
        return e, False


def arduino_connect(port):
    s = serial.Serial(port, baudrate=9600, timeout=2)
    time.sleep(3)
    return s


if __name__ == '__main__':
    if len(sys.argv) >= 2:
        if sys.argv[1] == "test":
            arduino = arduino_connect(data["arduinoport"])
            arduino_write(arduino, "test:lamp1")
    else:
        ports = list_serial_ports()
        for p in ports:
            print(p)
