"""
This module deals with face recongition system.
"""

import os
import cv2
import face_recognition
import numpy as np
import sys
import pickle


def get_encoded_faces():
    """
    looks through the faces folder and encodes all
    the faces

    :return: dict of (name, image encoded)
    """
    encoded = {}
    if not os.path.exists("faces"):
        os.mkdir("faces")
        print("faces directory does not exist but it is created. Please put in the images of your face in faces "
              "directory")
    for dirpath, dnames, fnames in os.walk("./faces"):
        for f in fnames:
            if f.endswith(".jpg") or f.endswith(".png"):
                face = face_recognition.load_image_file("faces/" + f)
                encoding = face_recognition.face_encodings(face)[0]
                encoded[f.split(".")[0]] = encoding
    print(encoded)
    return encoded


def unknown_image_encoded(img):
    """
    encode a face given the file name
    """
    face = face_recognition.load_image_file("faces/" + img)
    encoding = face_recognition.face_encodings(face)[0]

    return encoding


def classify_face(im):
    """
    will find all of the faces in a given image and label
    them if it knows what they are. This does not cache the file. 

    :param im: str of file path
    :return: list of face names
    """
    faces = get_encoded_faces()
    faces_encoded = list(faces.values())
    known_face_names = list(faces.keys())

    img = cv2.imread(im, 1)
    # img = cv2.resize(img, (0, 0), fx=0.5, fy=0.5)
    # img = img[:,:,::-1]

    face_locations = face_recognition.face_locations(img)
    unknown_face_encodings = face_recognition.face_encodings(img, face_locations)

    face_names = []
    for face_encoding in unknown_face_encodings:
        # See if the face is a match for the known face(s)
        matches = face_recognition.compare_faces(faces_encoded, face_encoding, tolerance=0.5)
        name = "Unknown"

        # use the known face with the smallest distance to the new face
        face_distances = face_recognition.face_distance(faces_encoded, face_encoding)
        best_match_index = np.argmin(face_distances)
        if matches[best_match_index]:
            name = known_face_names[best_match_index]

        face_names.append(name)

        for (top, right, bottom, left), name in zip(face_locations, face_names):
            # Draw a box around the face
            cv2.rectangle(img, (left - 20, top - 20), (right + 20, bottom + 20), (255, 0, 0), 2)

            # Draw a label with a name below the face
            cv2.rectangle(img, (left - 20, bottom - 15), (right + 20, bottom + 20), (255, 0, 0), cv2.FILLED)
            font = cv2.FONT_HERSHEY_DUPLEX
            cv2.putText(img, name, (left - 20, bottom + 15), font, 1.0, (255, 255, 255), 2)

    # Display the resulting image
    while True:

        cv2.imshow('Video', img)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            return face_names


def train_face_cache():
    """
    Function to store the face training cache
    """
    dict_face = get_encoded_faces()
    with open('faces_data.dat', 'wb') as f:
        pickle.dump(dict_face, f)
    print("Face Cache written.")


def classify_face_cached(im):
    """
    Uses face_data.dat file to recognize faces
    :param im: im - image object
    :returns: faces_name - list
    """
    img = None
    with open('faces_data.dat', 'rb') as f:
        faces = pickle.load(f)
    faces_encoded = list(faces.values())
    known_face_names = list(faces.keys())
    if type(im) == "string":
        img = cv2.imread(im, 1)
    else:
        open_cv_image = np.array(im)
        img = open_cv_image[:, :, ::-1].copy()
        # img = cv2.resize(img, (0, 0), fx=0.5, fy=0.5)
    # img = img[:,:,::-1]

    face_locations = face_recognition.face_locations(img)
    unknown_face_encodings = face_recognition.face_encodings(img, face_locations)

    face_names = []
    for face_encoding in unknown_face_encodings:
        # See if the face is a match for the known face(s)
        matches = face_recognition.compare_faces(faces_encoded, face_encoding, tolerance=0.5)
        name = "Unknown"

        # use the known face with the smallest distance to the new face
        face_distances = face_recognition.face_distance(faces_encoded, face_encoding)
        best_match_index = np.argmin(face_distances)
        if matches[best_match_index]:
            name = known_face_names[best_match_index]

        face_names.append(name)

        for (top, right, bottom, left), name in zip(face_locations, face_names):
            # Draw a box around the face
            cv2.rectangle(img, (left - 20, top - 20), (right + 20, bottom + 20), (255, 0, 0), 2)

            # Draw a label with a name below the face
            cv2.rectangle(img, (left - 20, bottom - 15), (right + 20, bottom + 20), (255, 0, 0), cv2.FILLED)
            font = cv2.FONT_HERSHEY_DUPLEX
            cv2.putText(img, name, (left - 20, bottom + 15), font, 1.0, (255, 255, 255), 2)

    cv2.imwrite("res.jpg", img)
    # Display the resulting image
    print(face_names)
    return face_names


if __name__ == '__main__':
    if sys.argv is not None:
        if sys.argv[1].endswith('.jpg') or sys.argv[1].endswith('.png'):
            if os.path.exists(sys.argv[1]):
                classify_face_cached(sys.argv[1])
            else:
                print("File does not exist.")
        else:
            print("Supply a file name with format .png or .jpg")
    else:
        train_face_cache()
