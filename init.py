"""
This the main module that is used to run the application
"""

from flask import Flask, redirect, url_for, render_template, request, session, jsonify
import arduino_controller as arduino
import time
from store import *
import identify_face as rec
from datetime import datetime as dt
from PIL import Image
import telegram_send
import arduinoPyFir

app = Flask(__name__, template_folder='template')
app.secret_key = os.urandom(12).hex()
s = None  # This variable is created for arduino
useAutoDoor = True
ServerTime = dt.now()


@app.route("/")
def index():
    # session["username"]="admin"
    # Un-comment the above line only for debugging purpose
    if "username" in session:  # Verify login
        try:
            x = tuple(list_devices_name_detailed())
            return render_template("index.html", x=x)
        except Exception as e:
            return render_template("index.html", error=e)
    else:
        return redirect(url_for("login"))


# setting / changing admin password:
# This will be changed in future update for now it is commented out.
@app.route("/change-admin-pass", methods=['GET', 'POS'])
def change_password():
    if "username" in session:
        if session["username"] == "admin":
            if request.method == "POST":
                p1 = request.form["password1"]
                p2 = request.form["password2"]
                if p1 == p2:
                    try:
                        change_password('admin', p1)
                    except Exception as e:
                        return render_template("change-admin-pass.html", error=e)
                else:
                    return render_template("change-admin-pass.html", error="password did not match")
    else:
        return redirect(url_for("login"))


'''
# This is not in use currently
@app.route("/register-users", methods=["POST", "GET"])
def register():
    user = str(request.form["username"])
    if request.method == "POST":
        paswd = request.form["password"]
        hashedpassword = sha256.encrypt(paswd)
        c, conn = connect_db()
        var = '\'' + user + '\''
        command = "select * from users where name=" + var
        c.execute(command)
        x = c.fetchall()
        print(command)
        c.execute(command, var)
        if int(len(x)) > 0:
            return render_template("register-users.html", error="username already taken")
        else:
            try:
                c.execute("insert into users (name,password) values (%s,%s)", (user, hashedpassword))
                conn.commit()
                c.close()
                conn.close()
                gc.collect()
                session['user'] = user
                return redirect(url_for("index"))
            except Exception as e:
                return str(e)
    else:
        if "user" in session:
            return redirect(url_for("index"))
        else:
            return render_template("register-users.html")
'''


# Settings page
@app.route("/settings", methods=['GET', 'POST'])
def settings_page():
    if "username" not in session:
        return redirect(url_for("login"))
    else:
        if request.method == "GET":
            config = getConfig()
            return render_template("settings.html", x=config)
        else:
            change_config(request.form['arduinoport'], request.form['debug'], request.form['port'],
                          request.form['use_telegram'], request.form['use_automatic_door'], request.form['use_https'])
            return redirect(url_for('index'))


@app.route('/addDevices', methods=['GET', 'POST'])
def add_devices():
    if "username" not in session:
        return redirect(url_for("login"))
    else:
        a = list_devices_name()
        if request.method == 'POST':
            devname = request.form['devname']
            pin = request.form['pinname']
            choice = request.form['choice']
            if choice == 'add':
                if devname not in a:
                    add_devices(devname, pin)
                    a = list_devices_name()
                    return render_template("addDevices.html", x=a)
                else:
                    return render_template('addDevices.html', error="The record already exist", x=a)
            else:
                if devname not in a:
                    a = list_devices_name()
                    return render_template('addDevices.html', x=a, error='Invalid Record.')
                else:
                    remove_device(devname)
                    a = list_devices_name()
                    return render_template("addDevices.html", x=a)
        else:
            # get request
            return render_template("addDevices.html", x=a)


# login page
@app.route("/login", methods=["POST", "GET"])
def login():
    if "username" in session:  # check if user is in the session
        return redirect(url_for("index", usr=session["username"]))
    else:
        try:
            if request.method == "POST":
                user = request.form['username']
                password = request.form['password']
                x = verify_user(user, password)
                if x is None:
                    return render_template("login.html", error="User name not found")
                elif x:
                    session["username"] = user
                    return redirect(url_for("index"))
                else:
                    return render_template("login.html", error="Wrong login Credentials!")
            else:
                return render_template("login.html")
        except Exception as e:
            return render_template("login.html", error=e)


@app.route("/api/im_verify", methods=["GET", "POST"])
def api_process_image():
    conf = getConfig()
    if request.method == "POST":
        file = request.files['image']
        print("Well Image reqest Passed")
        # Read the image via file.stream
        img = Image.open(file.stream)
        face = rec.classify_face_cached(img)
        try:
            if face is not None:
                if face[0] == 'Unknown':
                    telegram_send.send(messages=["Someone tried to use the face recogniton system"])
                else:
                    telegram_send.send(messages=[face[0] + " Was let inside the Home"])
                    if useAutoDoor:
                        if conf[6] == 'legacy':
                            stuff, flag = arduino.arduino_write(s, "Door:Unlock")
                        else:
                            arduinoPyFir.toggle_door(s, 'l')
                os.system('telegram-send --file \'res.jpg\' --caption \"log\"')
                return jsonify({'msg': 'Sucess', 'size': [img.width, img.height], 'face': face[0]})
            else:
                telegram_send.send(messages=["Someone tried to use the face recognition system"])
                os.system('telegram-send --file \'res.jpg\' --caption \"log\"')
                return jsonify({'msg': 'Success', 'size': [img.width, img.height], 'face': 'Unknown'})
        except IndexError:
            telegram_send.send(messages=["Someone tried to use the face recognition system"])
            os.system('telegram-send --file \'res.jpg\' --caption \"log\"')
            return jsonify({'msg': 'Success', 'size': [img.width, img.height], 'face:': 'Unknown'})
        except Exception:
            telegram_send.send(messages=["Someone tried to use the face recognition system"])
            x = os.system('telegram-send --file \'res.jpg\' --caption \"log\"')
            return jsonify({'msg': 'Success', 'size': [img.width, img.height], 'face:': 'Unknown'})


@app.route("/logout")
def logout():
    session.clear()
    return redirect(url_for("login"))


@app.route("/usage_summary/<date>")
def usage_summary(date):
    if date == 'present':
        graph('cur')
        my_dat = dt.now()
        str_my_dat = my_dat.strftime('%m-%Y')
        return render_template('usage_summary.html', date=str_my_dat)
    else:
        print(type(date))
        status = graph(str(date))
        if status is True:
            return render_template('usage_summary.html', date=date)
        else:
            return 'Data not found'


# Main Code that consist of Devices command
@app.route("/device/<device_name>/<status>")
def device_toggle(device_name, status):
    try:
        x = list_devices_name()
        if device_name not in x:
            return jsonify({'Error': 'Device not found'})
        else:
            stuff, flag = arduino.arduino_write(s, device_name + ":" + status)

            if flag:
                change_device_status(device_name, status)
                if stuff.find("error") or stuff.find("Error"):
                    return str(stuff)

    except Exception as e:
        return str(e)


@app.route('/lock_door')
def lock_door():
    if useAutoDoor:
        stuff, flag = arduino.arduino_write(s, "Door:Lock")
        return 'Door has been locked.'
    else:
        return 'Enable Automatic Doors to use this functionality'


# API Call Request
@app.route("/api/device/<device_name>/<status>")
def api_device_toggle(device_name, status):
    try:
        x = list_devices_name()
        if device_name not in x:
            return jsonify({'Error': 'Device not found'})
        else:
            arduino.arduino_write(s, device_name + ":" + status)
            change_device_status(device_name, status)
            return api_status()
    except Exception as e:
        json_dict = {'error': str(e)}
        return jsonify(json_dict)


@app.route("/api/status")
def api_status():
    try:
        devices = list(get_device_status())
        json_dict = dict()
        # json_dict['devices'] = devices
        # Convert devices tuple to JSON
        for i in devices:
            for j in i:
                json_dict[i[0]] = i[1]

        return jsonify(json_dict)
    except Exception as e:
        return jsonify(e)


@app.route('/api/get_all_device_list')
def myDevicesList():
    x = list_devices_name()
    data = dict()
    for i in range(len(x)):
        data[str(i)] = x[i]
    return jsonify(data)


@app.route('/api/lock_door')
def api_lock_door():
    conf = getConfig()
    if useAutoDoor:
        if conf[6] == 'legacy':
            stuff, flag = arduino.arduino_write(s, "Door:Lock")
        else:
            arduinoPyFir.toggle_door(s, 'l')
        return jsonify({'msg': 'door has been locked'})
    else:
        return jsonify({'msg': 'Automatic Door is not supported'})


@app.route('/plot_graph', methods=['POST', 'GET'])
def plot_graph():
    if request.method == 'POST':
        date = str(request.form["date"])
        match = date.split('-')
        if len(match[0]) == 2 and len(match[1]) == 4:
            if os.path.exists(f'./static/data/{date}.yaml'):
                return redirect(f'/usage_summary/{date}')
            else:
                return redirect(f'/usage_summary/present')
        else:
            return "pattern not matched"
    else:
        return render_template('plot_graph.html')


@app.route('/plot_regression')
def plot_regression():
    data, predDay, predDate = showRegressionFor30days()
    tupledata = tuple(data)
    return render_template('plotRegression.html', x=tupledata, predDate=predDate, predDay=predDay)
    

@app.route('/api/<name>/status')
def api_status_device(name):
    a = get_device_status(name)
    data = dict()
    data[name] = a
    return jsonify(data)


@app.route('/api/info/<name>')
def api_device_info(name):
    l = get_device_details(name)
    data = dict()
    data['device-info'] = l
    return jsonify(data)


# Error Handling Methods
@app.errorhandler(404)
def not_found(e):
    return "Webpage not found"


@app.errorhandler(403)
def permission_denied(e):
    return "Access Denied!"


@app.errorhandler(400)
def bad_request(e):
    return str(e)


@app.errorhandler(500)
def internal_error(e):
    return "Internal error: " + str(e)


@app.errorhandler(503)
def int_error(e):
    return "Internal error: " + str(e)


def main():
    global s
    time.sleep(3)
    print("Starting the app...")
    rec.train_face_cache()
    device_list = list_devices_name()
    data = dict()
    showRegressionFor30days()
    try:
        if not os.path.exists('./config.yaml'):
            print("Warning Creating a configuration File. Please configure it.")
            create_config_file()
        if not os.path.exists("./static/graphs"):
            os.mkdir('./static/graphs')
            print("graphs are stored in static/graphs")
        with open('config.yaml', 'r') as f:
            data = yaml.load(f, Loader=yaml.FullLoader)
        data['server_start_time'] = dt.now()
        with open('config.yaml', 'w') as f:
            yaml.dump(data, f)
        if data["legacy"]:
            s = arduino.connect(data["arduinoport"])
        else:
            s = arduinoPyFir.getBoard(data["arduinoport"])  # Arduino Device
        useAutoDoor = data['use-automated-door'] or True
        print(useAutoDoor)
        f.close()
    except:
        print("Could not connect to the Arduino device.")
        if not data['debug']:
            quit()

    try:
        if data['use-https']:
            if data['port'] == 'default':
                app.run(host='0.0.0.0', debug=bool(data['debug']), port=443, ssl_context=('server.crt', 'server.key'),
                        threaded=True)
            else:
                app.run(host='0.0.0.0', debug=bool(data['debug']), port=int(data['port']),
                        ssl_context=('server.crt', 'server.key'), threaded=True)
        else:
            if data['port'] == 'default':
                app.run(host='0.0.0.0', debug=bool(data['debug']), port=80, threaded=True)
            else:
                app.run(host='0.0.0.0', debug=bool(data['debug']), port=int(data['port']), threaded=True)
    finally:
        device_list = list_devices_name()
        for device in device_list:
            arduino.arduino_write(s, device + ":" + "off")
        change_all_device_status("off")


if __name__ == "__main__":
    main()
