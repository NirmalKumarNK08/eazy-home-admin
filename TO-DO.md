# TO DO list
1. Make interfacing Arduino simpler. *
2. Local voice recognition system integr    ation.*
3. Interface Raspberry PI GPIO.
4. User login to only let authorized people to use.
5. Integrate smart bulbs (Phillips Hue).*
6. Cleaning the code.*
7. Support user Extension.
8. Telegram Bot for controlling home (optional).
9. Mini-game (Tic-tac-toe) Extension example.
10. Fix the graph.
11. Web client app. (client-app repository/ in server).
12. Make telegram bot optional.*
13. Test Arduino messaging service (SMS).*
14. Asynchronous task to monitor sensors connected with another Arduino or another IOT Sensors*