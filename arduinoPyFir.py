"""
* For Experimental use. This will be removed.
* This will be user friendly mode. You must define pin number for each devices when configuring names.
* You need to install arduino Firmata Library and upload the Firamata Standard Sketch to use this feature.
* For configuration you just need to edit devices.yaml file using the Web interface.
* You can also edit the devices.yaml file but be sure to follow the format.
* This will be replaced soon.
"""
import pyfirmata
from store import list_devices_name, change_device_status, get_pin_number, get_device_status, getConfig
import sys
import traceback


def toggle_device(board, devname, status):
    l_devices = list_devices_name()
    if devname in l_devices:
        x = get_pin_number(devname)
        print(x)
        try:
            # board = pyfirmata.Arduino(conf[0])
            if status == 'on' and get_device_status(devname) == 'off':
                board.digital[x].write(1)
                print("device on")
            elif status == 'off' and get_device_status(devname) == 'on':
                board.digital[x].write(0)
                print("device off")
            else:
                print(f"Error: The device is already turned {status}")
                return f"Error: The device is already turned {status}"
            change_device_status(devname, status)
            return f'Info: Device turned {status} successfully', True
        except:
            traceback.print_exc()
    else:
        return 'Error: Device not found', False


def getBoard(port='/dev/ttyACM0'):
    board = pyfirmata.Arduino(port)
    return board


def toggle_door(board, cmd):
    conf = getConfig()
    door_pin = int(conf[7])
    try:
        if cmd == 'ul':
            board.digital[door_pin].write(1)
        else:
            board.digital[door_pin].write(0)
    except:
        return False


if __name__ == '__main__':
    if len(sys.argv) > 1:
        toggle_device(sys.argv[1], sys.argv[2])
